package com.dwlab.testdeviceinfoapp;

import android.database.Cursor;
import android.net.Uri;
import android.provider.Browser;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MobileBrowserUsageActivity extends AppCompatActivity {

    public final Uri BOOKMARKS_URI = Uri.parse("content://com.android.chrome.browser/history");
    public final String[] HISTORY_PROJECTION = new String[]{
            "_id", // 0
            "url", // 1
            "visits", // 2
            "date", // 3
            "bookmark", // 4
            "title", // 5
            "favicon", // 6
            "thumbnail", // 7
            "touch_icon", // 8
            "user_entered", // 9
    };
    public final int HISTORY_PROJECTION_TITLE_INDEX = 5;
    public final int HISTORY_PROJECTION_URL_INDEX = 1;

    TextView tvData;
    String s = "<b>Browser History:</b>\n";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_browser_usage);
        tvData = (TextView) findViewById(R.id.tv_data);
        //tvData.setText(getBrowserHistory());
        tvData.setText("This feature is removed by google after Android 6.0");
    }

    public String getBrowserHistory() {
        Cursor cursor = getContentResolver().query(BOOKMARKS_URI, null, null, null, null);

        while (cursor.moveToNext()) {
            s += "\n" + cursor.getString(HISTORY_PROJECTION_TITLE_INDEX) + " : " + cursor.getString(HISTORY_PROJECTION_URL_INDEX);
        }

        return s;
    }
}
