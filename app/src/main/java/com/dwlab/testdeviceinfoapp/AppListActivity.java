package com.dwlab.testdeviceinfoapp;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import java.util.List;

public class AppListActivity extends AppCompatActivity {

    TextView tvData;
    String s = "Application List:\n";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_list);

        Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ApplicationInfo> applicationInfo = getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);
        tvData = (TextView) findViewById(R.id.tv_data);

        getAppslist(applicationInfo);
    }

    private void getAppslist(final List<ApplicationInfo> applicationInfo) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (ApplicationInfo applicationInfoData : applicationInfo) {
                    s += "\n " + applicationInfoData.loadLabel(getPackageManager());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvData.setText(s);
                        }
                    });
                }
            }
        }).start();
    }
}
