package com.dwlab.testdeviceinfoapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnDeviceInfo;
    Button btnTelcomProvider;
    Button btnGoogleMaps;
    Button btnLocationAccess;
    Button btnRecentPlaces;
    Button btnContactBook;
    Button btnCallLogs;
    Button btnEmailAccess;
    Button btnGalleryAccess;
    Button btnAppsList;
    Button btnMobileBrowserUsage;
    Button btnEmailAccounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnDeviceInfo = (Button)findViewById(R.id.device_info);
        btnTelcomProvider = (Button)findViewById(R.id.telcom_provider);
        btnGoogleMaps = (Button)findViewById(R.id.google_maps);
        btnLocationAccess = (Button)findViewById(R.id.location_access);
        btnRecentPlaces = (Button)findViewById(R.id.recent_places);
        btnContactBook = (Button)findViewById(R.id.contact_book);
        btnCallLogs = (Button)findViewById(R.id.call_logs);
        btnEmailAccess = (Button)findViewById(R.id.email_access);
        btnGalleryAccess = (Button)findViewById(R.id.gallery_access);
        btnAppsList = (Button)findViewById(R.id.apps_list);
        btnMobileBrowserUsage = (Button)findViewById(R.id.mobile_browser_usage);
        btnEmailAccounts = (Button)findViewById(R.id.email_accounts);

        btnDeviceInfo.setOnClickListener(this);
        btnTelcomProvider.setOnClickListener(this);
        btnGoogleMaps.setOnClickListener(this);
        btnLocationAccess.setOnClickListener(this);
        btnRecentPlaces.setOnClickListener(this);
        btnContactBook.setOnClickListener(this);
        btnCallLogs.setOnClickListener(this);
        btnEmailAccess.setOnClickListener(this);
        btnGalleryAccess.setOnClickListener(this);
        btnAppsList.setOnClickListener(this);
        btnMobileBrowserUsage.setOnClickListener(this);
        btnEmailAccounts.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.device_info:
                Intent intDeviceInfo = new Intent(MainActivity.this, DeviceInfoActivity.class);
                startActivity(intDeviceInfo);
                break;
            case R.id.telcom_provider:
                Intent intTelcomProvider = new Intent(MainActivity.this, TelcomProviderActivity.class);
                startActivity(intTelcomProvider);
                break;
            case R.id.google_maps:
                Intent intGoogleMaps = new Intent(MainActivity.this, GoogleMapsActivity.class);
                startActivity(intGoogleMaps);
                break;
            case R.id.location_access:
                Intent intLocationAccess = new Intent(MainActivity.this, LocationAccessActivity.class);
                startActivity(intLocationAccess);
                break;
            case R.id.recent_places:
                Intent intRecentPlaces = new Intent(MainActivity.this, RecentPlacesActivity.class);
                startActivity(intRecentPlaces);
                break;
            case R.id.contact_book:
                Intent intContactBook = new Intent(MainActivity.this, ContactBookActivity.class);
                startActivity(intContactBook);
                break;
            case R.id.call_logs:
                Intent intCallLogs = new Intent(MainActivity.this, CallLogsActivity.class);
                startActivity(intCallLogs);
                break;
            case R.id.email_access:
                Intent intEmailAccess = new Intent(MainActivity.this, EmailAccessActivity.class);
                startActivity(intEmailAccess);
                break;
            case R.id.gallery_access:
                Intent intGalleryAccess = new Intent(MainActivity.this, GalleryAccessActivity.class);
                startActivity(intGalleryAccess);
                break;
            case R.id.apps_list:
                Intent intAppList = new Intent(MainActivity.this, AppListActivity.class);
                startActivity(intAppList);
                break;
            case R.id.mobile_browser_usage:
                Intent intMobileBrowserUsage = new Intent(MainActivity.this, MobileBrowserUsageActivity.class);
                startActivity(intMobileBrowserUsage);
                break;
            case R.id.email_accounts:
                Intent intEmailAccounts = new Intent(MainActivity.this, EmailAccountsActivity.class);
                startActivity(intEmailAccounts);
                break;
        }
    }
}
