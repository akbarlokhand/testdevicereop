package com.dwlab.testdeviceinfoapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class RecentPlacesActivity extends AppCompatActivity {

    TextView tvData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_places);
        tvData = (TextView) findViewById(R.id.tv_data);
        tvData.setText("This feature can be made available by tracking the user movement using the local location monitoring. Google does not provide any direct apis for this feature.");
    }
}
