package com.dwlab.testdeviceinfoapp;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.sun.mail.imap.Utility;

import java.io.InputStream;

public class GalleryAccessActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnChooser;
    ImageView ivImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_access);

        btnChooser = (Button) findViewById(R.id.btn_gallery_chooser);
        ivImage = (ImageView) findViewById(R.id.iv_gallery_pic);

        btnChooser.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , 1);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        if(resultCode == RESULT_OK) {
            Uri selectedImage = imageReturnedIntent.getData();
            boolean isFromCamera = false;
            Bitmap bitmapImage = decodeSampledBitmapFromUri(selectedImage, 1024, 1024, isFromCamera);
            ivImage.setImageBitmap(bitmapImage);
        }
    }

    public Bitmap decodeSampledBitmapFromUri(Uri imageUri, int reqWidth, int reqHeight, boolean isFromCamera) {

        try {
            // Get input stream of the image
            final BitmapFactory.Options options = new BitmapFactory.Options();
            InputStream iStream = getContentResolver().openInputStream(imageUri);

            // First decode with inJustDecodeBounds=true to check dimensions
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(iStream, null, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            InputStream iStream2 = getContentResolver().openInputStream(imageUri);

            Bitmap bitmap = BitmapFactory.decodeStream(iStream2, null, options);

            if(isFromCamera) {
                int rotation = getCurrentRotation(imageUri);

                Log.d("Utility", "rotation--"+rotation);

                Matrix matrix = new Matrix();
                matrix.postRotate(rotation);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            }
            return bitmap;
        } catch(Exception e) {
            Log.d("Utilty", "decodeSampledBitmapFromUri: "+e.getMessage());
        }
        return null;
    }

    /**
     * Get the rotation of the last image added.
     * @param selectedImage
     * @return
     */
    private int getCurrentRotation(Uri selectedImage) {
        int rotation =0;
        ContentResolver content = getContentResolver();


        Cursor mediaCursor = content.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[] { "orientation", "date_added" },null, null,"date_added desc");

        if (mediaCursor != null && mediaCursor.getCount() !=0 ) {
            while(mediaCursor.moveToNext()){
                rotation = mediaCursor.getInt(0);
                break;
            }
        }
        mediaCursor.close();
        return rotation;
    }

    private static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
