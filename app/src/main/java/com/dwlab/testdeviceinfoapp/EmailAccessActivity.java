package com.dwlab.testdeviceinfoapp;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;

public class EmailAccessActivity extends AppCompatActivity {

    TextView tvData;
    Button btnGetInboxData;

    EditText edtUserName;
    EditText edtPassword;

    String mUsername = null;
    String mPassword = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_access);
        tvData = (TextView) findViewById(R.id.tv_data);
        edtUserName = (EditText) findViewById(R.id.edt_email_username);
        edtPassword = (EditText) findViewById(R.id.edt_email_password);

        btnGetInboxData = (Button) findViewById(R.id.btn_get_inbox_data);

        btnGetInboxData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(edtUserName.getText().toString()) && TextUtils.isEmpty(edtPassword.getText().toString())) {
                    Toast.makeText(EmailAccessActivity.this, "Please enter username and password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                mUsername = edtUserName.getText().toString();
                mPassword = edtPassword.getText().toString();
                UIUtility.showProgress("Fetching Emails..", EmailAccessActivity.this);
                new FetchPop().execute();
            }
        });
    }

    public class FetchPop extends AsyncTask<String, Void, Void> {

        public void fetch(String pop3Host, String storeType, String user,
                          String password) {
            try {
                // create properties field
                Properties properties = new Properties();
                properties.put("mail.store.protocol", "pop3");
                properties.put("mail.pop3.host", pop3Host);
                properties.put("mail.pop3.port", "995");
                properties.put("mail.pop3.starttls.enable", "true");
                Session emailSession = Session.getDefaultInstance(properties);
                // emailSession.setDebug(true);

                // create the POP3 store object and connect with the pop server
                Store store = emailSession.getStore("pop3s");

                store.connect(pop3Host, user, password);

                // create the folder object and open it
                Folder emailFolder = store.getFolder("INBOX");
                emailFolder.open(Folder.READ_ONLY);

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        System.in));

                // retrieve the messages from the folder in an array and print it
                Message[] messages = emailFolder.getMessages();
                Log.d("No. messages:", messages.length + ""); //just the number at first

                final StringBuilder sb = new StringBuilder();
                sb.append("<h4>Email Inbox Details <h4>");
                sb.append("\n");
                sb.append("\n");

                sb.append("<table>");
                int msgLength = 1;
                if (messages.length > 10) {
                    msgLength = 10;
                }

                for (int i = 0; i < msgLength; i++) {
                    Message message = messages[i];

                    sb.append("<tr>")
                            .append("<td>Email Number:</td>")
                            .append("<td><strong>")
                            .append((i + 1))
                            .append("</strong></td>");
                    sb.append("</tr>");
                    sb.append("<br/>");
                    sb.append("<tr>")
                            .append("<td>Subject:</td>")
                            .append("<td><strong>")
                            .append(message.getSubject())
                            .append("</strong></td>");
                    sb.append("</tr>");
                    sb.append("<br/>");
                    sb.append("<tr>")
                            .append("<td>From:</td>")
                            .append("<td><strong>")
                            .append(message.getFrom()[0])
                            .append("</strong></td>");
                    sb.append("</tr>");
                    sb.append("<br/>");
                    sb.append("<tr>")
                            .append("<td>Text:</td>")
                            .append("<td><strong>")
                            .append(message.getContent().toString())
                            .append("</strong></td>");
                    sb.append("</tr>");
                    sb.append("<br/>");
                    sb.append("<br/>");
                }
                sb.append("</table>");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvData.setText(Html.fromHtml(sb.toString()));
                    }
                });

/*for (int i = 0; i < messages.length; i++) {
                Message message = messages[i];
                writePart(message);
                String line = reader.readLine();
                if ("YES".equals(line)) {
                    message.writeTo(System.out);
                } else if ("QUIT".equals(line)) {
                    break;
                }
            }*/
                // close the store and folder objects
                emailFolder.close(false);
                store.close();

            } catch (NoSuchProviderException e) {
                e.printStackTrace();
            } catch (MessagingException e) {
                e.printStackTrace();
            } /*catch (IOException e) {
            e.printStackTrace();
        }*/ catch (Exception e) {
                e.printStackTrace();
            } finally {
                UIUtility.dismissDialog();
            }
        }


        @Override
        protected Void doInBackground(String... params) {
            String host = "pop.gmail.com";// I tried google's pop
            String mailStoreType = "pop3";

            //Call method fetch
            fetch(host, mailStoreType, mUsername+"@gmail.com", mPassword);
            Log.d("mytag", "done!");
            return null;
        }

        public void GO() {
            doInBackground(null);
        }


        /*
        * This method checks for content-type
        * based on which, it processes and
        * fetches the content of the message
        */
        public void writePart(Part p) throws Exception {
            if (p instanceof Message)
                //Call methos writeEnvelope
                writeEnvelope((Message) p);

       /* System.out.println("----------------------------");
        System.out.println("CONTENT-TYPE: " + p.getContentType());*/

            //check if the content is plain text
            if (p.isMimeType("text/plain")) {
                System.out.println("This is plain text");
                System.out.println("---------------------------");
                System.out.println((String) p.getContent());
            }
            //check if the content has attachment
            else if (p.isMimeType("multipart/*")) {
                System.out.println("This is a Multipart");
                System.out.println("---------------------------");
                Multipart mp = (Multipart) p.getContent();
                int count = mp.getCount();
                for (int i = 0; i < count; i++)
                    writePart(mp.getBodyPart(i));
            }
            //check if the content is a nested message
            else if (p.isMimeType("message/rfc822")) {
                System.out.println("This is a Nested Message");
                System.out.println("---------------------------");
                writePart((Part) p.getContent());
            }
            //check if the content is an inline image
            else if (p.isMimeType("image/jpeg")) {
                System.out.println("--------> image/jpeg");
                Object o = p.getContent();

                InputStream x = (InputStream) o;
                // Construct the required byte array
                int i;
                byte[] bArray = new byte[0];
                System.out.println("x.length = " + x.available());
                while ((i = (int) ((InputStream) x).available()) > 0) {
                    int result = (int) (((InputStream) x).read(bArray));
                    if (result == -1)
                        i = 0;
                    bArray = new byte[x.available()];

                    break;
                }
                FileOutputStream f2 = new FileOutputStream("/tmp/image.jpg");
                f2.write(bArray);
            } else if (p.getContentType().contains("image/")) {
                System.out.println("content type" + p.getContentType());
                File f = new File("image" + new Date().getTime() + ".jpg");
                DataOutputStream output = new DataOutputStream(
                        new BufferedOutputStream(new FileOutputStream(f)));
                com.sun.mail.util.BASE64DecoderStream test =
                        (com.sun.mail.util.BASE64DecoderStream) p
                                .getContent();
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = test.read(buffer)) != -1) {
                    output.write(buffer, 0, bytesRead);
                }
            } else {
                Object o = p.getContent();
                if (o instanceof String) {
                    System.out.println("This is a string");
                    System.out.println("---------------------------");
                    System.out.println((String) o);
                } else if (o instanceof InputStream) {
                    System.out.println("This is just an input stream");
                    System.out.println("---------------------------");
                    InputStream is = (InputStream) o;
                    is = (InputStream) o;
                    int c;
                    while ((c = is.read()) != -1)
                        System.out.write(c);
                } else {
                    System.out.println("This is an unknown type");
                    System.out.println("---------------------------");
                    System.out.println(o.toString());
                }
            }

        }

        /*
        * This method would print FROM,TO and SUBJECT of the message
        */
        public void writeEnvelope(Message m) throws Exception {
            System.out.println("This is the message envelope");
            System.out.println("---------------------------");
            Address[] a;

            // FROM
            if ((a = m.getFrom()) != null) {
                for (int j = 0; j < a.length; j++)
                    System.out.println("FROM: " + a[j].toString());
            }

            // TO
            if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
                for (int j = 0; j < a.length; j++)
                    System.out.println("TO: " + a[j].toString());
            }

            // SUBJECT
            if (m.getSubject() != null)
                System.out.println("SUBJECT: " + m.getSubject());

        }

    }
}
