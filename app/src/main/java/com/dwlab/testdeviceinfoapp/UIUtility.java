package com.dwlab.testdeviceinfoapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

/**
 * Created by Akbar on 6/3/2016.
 */

public class UIUtility {

    private static ProgressDialog pDialog;
    public static void showProgress(String message, Context context) {
        if(pDialog != null && pDialog.isShowing()) {
            return;
        }
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(message);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public static void dismissDialog() {
        if(pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    public static void showToast(String message, Context context, int duration) {
        Toast.makeText(context, message, duration).show();
    }

    public static void showAlert(Context context, String message, DialogInterface.OnClickListener dialogPositiveClick) {
        new AlertDialog.Builder(context)
                .setTitle("Alert")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, dialogPositiveClick)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

}
