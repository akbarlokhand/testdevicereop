package com.dwlab.testdeviceinfoapp;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DeviceInfoActivity extends AppCompatActivity {

    TextView tvData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_info);
        tvData = (TextView)findViewById(R.id.tv_data);
        tvData.setText(getDeviceInfo());
    }

    private String getDeviceInfo() {
        String s="Debug-info:";
        s += "\n OS Version: " + System.getProperty("os.version") + "(" + Build.VERSION.INCREMENTAL + ")";
        s += "\n OS API Level: " + Build.VERSION.SDK_INT;
        s += "\n Device: " + Build.DEVICE;
        s += "\n Model: " + Build.MODEL;
        s += "\n Product: " + Build.PRODUCT;
        s += "\n Board: " + Build.BOARD;
        s += "\n Bootloader: " + Build.BOOTLOADER;
        s += "\n Brand: " + Build.BRAND;
        s += "\n Device: " + Build.DEVICE;
        s += "\n Hardware: " + Build.HARDWARE;
        s += "\n Manufacturer: " + Build.MANUFACTURER;
        return s;
    }
}
